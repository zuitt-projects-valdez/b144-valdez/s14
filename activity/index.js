// short one line comment
/* multiline comment (VSC) option + shift + a */

console.log("Hello Batch 144!");

/* 
  Javascript - can see or log messages in our console 

  browser consoles are part of our browsers which will allow us to see/log messages, data or information from our programming language - javascript 

  for most browsers, consoles are easily accessed through its dev tools in the console tab. 

  consoles in browsers will allow us to add some js expressions 

  statements 
    instructions, expressions that we add to our programming language to communicate with our computers

    js statements usually ends in a semi colon. however js has implemented a way to automatically add semicolons at the end of a line/statement. semi colons can actually be omitted in creating js statements but semicolons in js are added to mark the end of the statement

    syntax
      synatx in programming is a set of rules that describes how statements are properly made/constructed.

      lines/blocks of code must follow a certain set of rules for it to work properly.

      (ex. console.log, let, const, etc.)
*/

console.log("Veronica");

// Mini activity

/* console.log("arroz caldo");
console.log("arroz caldo");
console.log("arroz caldo");
 */
let food1 = "Arroz Caldo";

console.log(food1);

/* 
  Variables 
    way to store data or info within our js 

    to create a variable we first declare the name of the variable with either the let/const keyword

      let nameOfVariable

      then we can initialize the variable with a value or data 

      let nameOfVariable = "data"

*/

console.log("My favorite food is: " + food1);

let food2 = "Tinola";

console.log("My favorite foods are: " + food1 + " and " + food2);

let food3;
console.log(food3);

/* 
not defined = error; undefined = not error

we can create variables without adding an initial value, however, the variable's content is undefined 

*/

food3 = "Chickenjoy";

console.log(food3);

/* can update the content of a let variable by reassigning the content using an assignment operator 

(=) - assignment operator; lets us assign data to a variable 
 */

// Mini activity

food1 = "Bulalo";
food2 = "Sisig";

console.log(food1);
console.log(food2);

/* 
  we can update our variables with an assignment operator without needing to use the let keywork again 

  let food1 = "flat"; -- redefining a variable this way will lead to an error
*/

/* 
  const 
    creates variable that are constant 
    the data in these variables will not change, cannot be changed, and should not be changed
*/

const pi = 3.1416;

console.log(pi);

/* You cannot reassign constant (const) variables
pi = "pizza";
console.log(pi); */

/* 
const gravity;

console.log(gravity); 

cannot declare/creat a const variable without initialization or an initial value 
*/

let myName;
const sunriseDirection = "East";
const sunsetDirection = "West";

console.log(myName, sunriseDirection, sunsetDirection);

/* 
  Guides in creating a JS Variable:
  1. We can create a let variable with the let keyword; let vars can be reassigned but not redeclared

  2. Creating a variable has two parts: declaration of the variable name and initialization of the initial value of the variable using an assignment operator

  3. A let variable can be declared without initialization. The value of the variable will be undefined until it is reassigned with a value as a result

  4. Not Defined vs Undefined 
    not defined - variable is used but not declared 
    undefined - variable that is used and declared but is not initialized 

  5. Can use const keyword to create constant variables. Const variables cannot be declared without initialization. Const vars cannot be reassigned. 

  6. When creating variable names start with small caps to avoid conflict with syntax in JS that is named with capital letters. 

  7. If the variable would need 2 words, the naming convention is the use of camelCase. Do not add a space for vars with 2 words as names. 
*/

/* 
  Data types 
  - string data type 
      data which are alphanumerical text
      it could be a name, phrase, or sentence
      not limited to letters 
      can create string with either ' ' or " "
      strings are data not variables 

      variable = "string data"
*/

let country = "Philippines";
let province = "Rizal";

console.log(province, country);

/* 
  Concatenation
    a way for us to add strings together and have a single string 

    use (+) to concatenate data 
*/

let fullAddress = province + ", " + country;

console.log(fullAddress);

let greeting = "I live in " + country;

console.log(greeting);

// in JS, using + with strings is concatenation

let numString = "50";
let numString2 = "25";

console.log(numString + numString2);

/* 
  Strings have a property called (.length)
    tells us the number of characters in a string 
    spaces are also counted as string characters 
    returns number type data 
*/
let hero = "Captain America";

console.log(hero.length);

/* 
  Data types 
    - string 
    - number type data 
        can be used in proper mathematical equations and operations 

*/

let students = 16;
console.log(students);

let num1 = 50;
let num2 = 25;

/* 
  Addition operator adds two number types and return the sum as a number data type
  
    operations return a value that can be saved as a variable  

    addition operator on a number type and numerical string returns a concatenated string 

  parseInt() - converts numerical strings to a number type 
*/

console.log(num1 + num2);

let sum1 = num1 + num2;
console.log(sum1);

let numString3 = "100";
console.log(num1 + numString3);

let sum2 = parseInt(numString3) + num1;
console.log(sum2);

/* const firstName = "Jeff";
console.log(firstName, sunriseDirection, sunsetDirection);
console.log(num1, firstName);
console.log(num1, numString3); */

// the types of data will look different (diff colors) in the console to distinguish the different data types (string will look different color than default when it is in the same line or logged at the same time as a different data type  )

let sum3 = sum1 + sum2;
let sum4 = parseInt(numString2) + num2;

console.log(sum3, sum4);

/* 
  Subtraction operator 
  - will let us get the difference between two number types. 
  - results in number type data 
  - subtraction operator will convert a numerical string automatically and perform the operation as normal 
      type conversion/type coercion/forced coercion
      - automatic conversion from one type to another
   - text string - number = NaN (not a number)
      js cannot convert a text string to a number 
*/

console.log(num1 - num2);

let difference = num1 - num2;
console.log(difference);

let difference2 = numString3 - num2;
console.log(difference2);

let difference3 = hero - num2;
console.log(difference3);

let string1 = "fifteen";
console.log(string1 - num2); // this does not work because string1 is still a string

/* 
  Multiplication operator
*/
let num3 = 10;
let num4 = 5;

let product = num3 * num4;
console.log(product);

let product2 = numString3 * num3;
console.log(product2);

let product3 = numString * numString3;
console.log(product3);

// Division operator
let num5 = 30;
let num6 = 3;
let quotient = num5 / num6;
console.log(quotient);

let quotient2 = numString3 / 5;
console.log(quotient2);

let quotient3 = 450 / num4;
console.log(quotient3);

/* 
  Data types:
  - string 
  - number 
  - boolean (true or false)
    - used for logical operations or for if/else conditions 
    - naming convention for a variable that contains boolean is a yes or no question
*/

let isAdmin = true;
let isMarried = false;

/* 
  Arrays
  - special kind of data type wherein we can store multiple values 
  - an array can store multiple values of different types 
  - best practice: keep data types of the array uniform 
  - values in an array are separated by a comma (results in error if not)
  - created with an array literal ([])
  - array indices
    - number of the array item's order
    - markers of the order of the items in the array 
    - indices start at 0 
*/

let koponanNiEugene = ["Eugene", "Alfred", "Dennis", "Vincent"];
console.log(koponanNiEugene);

//accessing an array item: arrayName[index];
console.log(koponanNiEugene[0]);

//bad practice - not descriptive
let array2 = ["One Punch Man", true, 500, "Saitama"];
console.log(array2);

/* 
  Objects 
  - special data type used to mimic real world objects 
  - can add information that makes sense, is thematically relevant, and of different data types
  - created with object literals ({}) 
  - each value is given a label which makes the data more significant = Key-Value pair 
    - key = label 
    - value = data
    - key-value pair = property
    - properties are separated by commas 
*/

let person1 = {
  heroName: "One Punch Man",
  isRegistered: true,
  salary: 500,
  realName: "Saitama",
};

// accessing a value of an object's property - dot notation; objectName.propertyName
console.log(person1.realName);

//Mini activity
let myFavoriteBands = ["Paramore", "Free Nationals", "Silk Sonic"];

let me = {
  firstName: "Veronica",
  lastName: "Valdez",
  isWebDeveloper: true,
  hasPortfolio: false,
  age: 23,
};

/* 
  Data types:
  - string
  - number
  - boolean
  - array
  - object
  - undefined 
  - null 

  Undefined vs Null 
    Null 
    - explicit declaration that there is no value at all
    - certain processes in programming returns null to indicate that the task resulted to nothing 

    Undefined
    - variable exists however a value was not initialized with the variable during declaration
    - a value has yet to assigned to the variable 
    - normally caused by developers creating variables that have no value or data associated with them 
      - variable exists but its value is still unknown 

*/

let sampleNull = null;

let undefinedSample;
console.log(undefinedSample);

let foundResult = null;

let person2 = {
  firstName: "Patricia",
  age: 28,
};

//variable (person2) exists but the property (isAdmin) does not yet
console.log(person2.isAdmin);
