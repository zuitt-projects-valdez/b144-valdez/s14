// FUNCTIONS

function printStar() {
  console.log("*");
  console.log("**");
  console.log("***");
}

/* printStar();
printStar();
printStar();
printStar(); */

function sayHello(name) {
  console.log("Hello" + name);
}

/* sayHello(6); // Js converts this to a string and concats the console statement  */

function alertPrint() {
  // alert("Hello");
  console.log("Hello");
}

// alertPrint();

// Function calling a function (function in a function)
/* function alertPrint() {
  alert("Hello");
  sayHello("Hello");
}

alertPrint(); */

/* Function that accepts two numbers and prints the sum */

// Parameters - additional info for function to work
// parameters should have the same name in the function
/* function addSum(num1, num2) {
  let sum = num1 + num2;
  console.log(sum);
} */

/* addSum(13, 2); //arguments - actual values in the function
addSum(23, 56); */

// Function with 3 parameters
function printBio(lName, fName, age) {
  // console.log("Hello " + lName + fName + age);

  /*   String template literals
      interpolation (${variable}) */

  console.log(`Hello Ms. ${lName} ${fName} ${age}`);
}

// printBio("Valdez", "Veronica", 23);

function addSum(x, y) {
  return y - x; //statements after the return statement will not be executed because the values have already been returned
  console.log(x + y);
}

/* return keyword - returns the result or value of the function; won't see where it is so you have to capture the return value with a variable in order to use it */
let sumNum = addSum(3, 4);
// addSum(3, 4);
